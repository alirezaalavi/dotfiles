-----------------------------
--         GLOBAL       -----
-----------------------------
-- Break inserted text into smaller undo units when we insert some punctuation chars.
local undo_ch = { ",", ".", "!", "?", ";", ":" }
for _, ch in ipairs(undo_ch) do
    vim.keymap.set("i", ch, ch .. "<c-g>u")
end

vim.g.mapleader = " "
vim.keymap.set("n", "<leader>pv", vim.cmd.Ex, { desc = "open Ex" })

-- Turn the word under cursor to uppercase
-- vim.keymap.set("i", "<c-u>", "<Esc>gUiww")
vim.keymap.set("i", "<c-u>", "<Esc>gUiwea")
vim.keymap.set("n", "<leader><c-u>", "<Esc>gUiww", { desc = "UpperCase Current Word" })
-- Turn the current line into title case
-- From https://stackoverflow.com/questions/17440659/capitalize-first-letter-of-each-word-in-a-selection-using-vim
vim.keymap.set("n", "<leader><c-t>", ":s/\\<./\\u&/g", { desc = "TitleCase Current Line" })

-- Insert a blank line below or above current line (do not move the cursor),
-- see https://stackoverflow.com/a/16136133/6064933
vim.keymap.set("n", "<leader>o", "o<ESC>k", {
    desc = "insert line below",
})

vim.keymap.set("n", "<leader>O", "O<ESC>j", {
    desc = "insert line above",
})

-- Buffer keymaps
vim.keymap.set("n", "<leader>by", "<cmd>%yank<cr>", { desc = "Yank entire buffer" })
vim.keymap.set("n", "<leader>bd", "<cmd>%delete<cr>", { desc = "Delete entire buffer" })

-- Move the highlighted region up or down
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")
-- Add comma at end of line
vim.keymap.set("n", "<leader>,", "A,<ESC>")
-- Add semicolon at the end of line
vim.keymap.set("n", "<leader>;", "A;<ESC>")

-- Center the line when scrolling
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")

-- Center the search term when searching
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

-- Clipboard actions
vim.keymap.set("x", "<leader><C-p>", [["_dP]], { desc = "Overwrite selection" })
vim.keymap.set({ "n", "v" }, "<leader><C-p>", [["+P]], { desc = "Paste from system clipboard" })
vim.keymap.set({ "n", "v" }, "<leader>y", [["+y]], { desc = "Yank to system clipboard" })
vim.keymap.set("n", "<leader>Y", [["+Y]], { desc = "Yank line to system clipboard" })
vim.keymap.set({ "n", "v" }, "<leader>d", [["_d]], { desc = "Delete to void" })

vim.keymap.set("n", "<leader><C-h>", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]], { desc = "Replace word" })

-----------------------------
--      IF VSCODE       -----
-----------------------------
if vim.g.vscode then
    vim.keymap.set("n", "<leader>w", "<cmd>call VSCodeNotify('workbench.action.files.save')<cr>",
        { silent = true, desc = "save buffer" })
    vim.keymap.set("n", "<leader>q", "<cmd>call VSCodeNotify('workbench.action.closeActiveEditor')<cr>",
        { desc = "write and quit buffer" })
    -- TODO add this to normal neovim with harpoon
    vim.keymap.set("n", "?", "<Cmd>call VSCodeNotify('workbench.action.findInFiles', { 'query': expand('<cword>')})<CR>")
    vim.keymap.set("n", "gcc", "<cmd>call VSCodeNotify('editor.action.commentLine')<cr>")
    vim.keymap.set({ "n", "v" }, "gbc", "<cmd>call VSCodeNotify('editor.action.blockComment')<cr>")
    vim.keymap.set("n", "<leader>f", "<cmd>call VSCodeNotify('editor.action.formatDocument')<cr>")
else
    -----------------------------
    --      IF NeoVim       -----
    -----------------------------
    vim.keymap.set("n", "<leader>w", "<cmd>update<cr>", { silent = true, desc = "save buffer" })
    vim.keymap.set("", "<F5>", ":MundoToggle<CR>")
    vim.keymap.set("n", "<leader>q", "<cmd>q<cr>", { silent = true, desc = "quit buffer" })
end
