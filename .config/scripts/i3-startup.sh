#!/bin/bash

# Run startup commands (not applications. applications should be started with exec in i3 config)
autorandr --change;
sleep 2;
nitrogen --restore;
sleep 1;
picom -b;
$HOME/.config/scripts/start-conky.sh;
$HOME/.config/scripts/config-setxkbmap.sh

exit
