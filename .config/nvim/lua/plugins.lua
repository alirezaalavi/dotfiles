-- This file can be loaded by calling `lua require('plugins')` from your init.vim
local fn = vim.fn

local utils = require("utils")

-- Automatically install packer
local install_path = fn.stdpath "data" .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
    PACKER_BOOTSTRAP = fn.system {
        "git",
        "clone",
        "--depth",
        "1",
        "https://github.com/wbthomason/packer.nvim",
        install_path,
    }
    print "Installing packer close and reopen Neovim..."
    vim.cmd [[packadd packer.nvim]]
end

-- Autocommand that reloads neovim whenever you save the plugins.lua file
vim.cmd [[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]]

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
    return
end
------------------------------
--      DEFINE PLUGINS    ----
------------------------------
return packer.startup(function(use)
    -- it is recommended to put impatient.nvim before any other plugins
    use { "lewis6991/impatient.nvim", config = [[require('impatient')]] }
    -- packer can manage itself
    use { "wbthomason/packer.nvim" }


    -- Automatic insertion and deletion of a pair of characters
    use { "Raimondi/delimitMate",
        cond = function() return not vim.g.vscode end,
    }

    -- surround text objects
    use { 'machakann/vim-sandwich' }
    -- Modern matchit implementation
    use {
        "andymass/vim-matchup",
        event = "VimEnter",
        cond = function() return not vim.g.vscode end,
    }


    ----------------------------
    --  Only in plain neovim   --------
    ----------------------------
    -- Show undo history visually
    use {
        "simnalamburt/vim-mundo",
        cmd = { "MundoToggle", "MundoShow" },
        cond = function() return not vim.g.vscode end,
    }

    use {
        "JoosepAlviste/nvim-ts-context-commentstring",
        lazy = true,
        cond = function() return not vim.g.vscode end,
    }
    -- Comment plugin
    use {
        'numToStr/Comment.nvim',
        --event = "BufRead",
        config = function()
            require('Comment').setup {
                pre_hook = require('ts_context_commentstring.integrations.comment_nvim').create_pre_hook(),
            }
        end,
        cond = function() return not vim.g.vscode end,
    }

    -- highlighting todo , fix, bug etc. comments
    use {
        "folke/todo-comments.nvim",
        dependencies = { "nvim-lua/plenary.nvim" },
        config = function()
            require('todo-comments').setup()
        end,
        opts = {
            -- your configuration comes here
            -- or leave it empty to use the default settings
            -- refer to the configuration section below
        },
        cond = function() return not vim.g.vscode end,
    }
    -------------------------
    --  THEMES          -----
    -------------------------
    use {
        "catppuccin/nvim",
        as = "catppuccin",
        cond = function() return not vim.g.vscode end,
    }
    -- use {
    --     "Mofiqul/dracula.nvim",
    --     as = "dracula",
    --     cond = function() return not vim.g.vscode end,
    -- }
    -- showing keybindings
    use {
        "folke/which-key.nvim",
        -- event = "VimEnter"
        cond = function() return not vim.g.vscode end,
    }

    -- Since tmux is only available on Linux and Mac, we only enable these plugins
    -- for Linux and Mac
    if utils.executable("tmux") then
        -- .tmux.conf syntax highlighting and setting check
        use {
            "tmux-plugins/vim-tmux",
            ft = { "tmux" },
            cond = function() return not vim.g.vscode end,
        }
    end

    -- better UI for some nvim actions
    use {
        "stevearc/dressing.nvim",
        cond = function() return not vim.g.vscode end,
    }

    -- colorize color codes
    use {
        'norcalli/nvim-colorizer.lua',
        config = function() require('colorizer').setup() end,
        cond = function() return not vim.g.vscode end,
    }

    -- treesitter
    use {
        "nvim-treesitter/nvim-treesitter",
        run = ":TSUpdate",
        cond = function() return not vim.g.vscode end,
    }
    use {
        "nvim-treesitter/nvim-treesitter-context",
        cond = function() return not vim.g.vscode end,
    }

    -- Git plugin
    use {
        "tpope/vim-fugitive",
        cond = function() return not vim.g.vscode end,
    }

    -- Fuzzy finder
    use {
        'nvim-telescope/telescope.nvim', branch = '0.1.x',
        requires = { {
            'nvim-lua/plenary.nvim',
            cond = function() return not vim.g.vscode end,
        } },
        cond = function() return not vim.g.vscode end,
    }
    use {
        'ThePrimeagen/harpoon',
        cond = function() return not vim.g.vscode end,
    }
    -- LSP
    use {
        'VonHeikemen/lsp-zero.nvim',
        branch = 'v2.x',
        requires = {
            -- LSP Support
            {
                'neovim/nvim-lspconfig',
                cond = function() return not vim.g.vscode end,
            }, -- Required
            {
                -- Optional
                'williamboman/mason.nvim',
                cond = function() return not vim.g.vscode end,
            },
            {
                'williamboman/mason-lspconfig.nvim',
                cond = function() return not vim.g.vscode end,
            }, -- Optional

            -- Autocompletion
            {
                'hrsh7th/nvim-cmp',
                cond = function() return not vim.g.vscode end,
            }, -- Required
            {
                'hrsh7th/cmp-nvim-lsp',
                cond = function() return not vim.g.vscode end,
            }, -- Required
            {
                'L3MON4D3/LuaSnip',
                cond = function() return not vim.g.vscode end,
            }, -- Required
            {
                'hrsh7th/cmp-buffer',
                cond = function() return not vim.g.vscode end,
            },
        },
        cond = function() return not vim.g.vscode end,
    }
    -- Automatically set up your configuration after cloning packer.nvim
    -- Put this at the end after all plugins
    if PACKER_BOOTSTRAP then
        require('packer').sync()
    end
end)
