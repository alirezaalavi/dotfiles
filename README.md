# My Linux Workflow Configs

## Screenshots
![Dracula theming](.config/screenshots/i3-dracula_1.png)
![Dracula theming](.config/screenshots/i3-dracula_2.png)
![Dracula theming](.config/screenshots/i3-dracula_3.png)

## Introduction
Here I want to keep my personal config files, both for myself to not lose them and for others that might find them useful.
### Details
I am a software engineer, so these configs(specially i3 configs) are made for easy developer workflow, alongside aesthetics.
I use a Lenovo Legion laptop with intel CPU and NVIDIA GPU, and use dual monitors at work, so the needed configs are mostly available here.
## What configs are available?

- i3
- dunst
- picom
- urxvt
- bash
- zsh
- fish
- conky
- i3status-rust
- alacritty
- tmux
- rofi
- dmenu
- VsCode (configured for vscode-neovim)
- Neovim
- Alacritty
- fish
- pulse
- qutebrowser
- youtube-dl
- .Xresources
- Wezterm

## Theming
Apply themes (kvantum, gtk, etc.) manually and recheck the configs related to them, also check if the needed files are available on
your system (`~/.themes, ~/.icons, /usr/share/themes, etc.`)
## Install scripts
**These scripts are created for Arch and Arch based linux distros.
And are not general as I've created them for myself. So it's best to read and understand what they are doing first**
### install-apps.sh
Installs my essential apps available in arch repos
### install-apps-extra.sh
Installs extra apps
### install-apps-AUR.sh
Sets up YAY and installs basic apps from AUR

## TODOs
[] Split i3 config into different files with v4

[] Add leader active indicator to wezterm

[] Make wezterm cd into parent's CWD when splitting

[] Change wezterm tab titles based on running program and zoom state indicator

[] Move configs the [chezmoi](https://www.chezmoi.io/)

## How To Build Dotfiles repo
This repo was created with the guidelines in [this](https://www.atlassian.com/git/tutorials/dotfiles) tutorial.
In addition to the steps in above tutorial, also run:
`config config --local status.showUntrackedFiles no`
So untracked files (files that you don't want in your dotfiles) dont clutter your repo.
