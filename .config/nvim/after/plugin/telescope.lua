local builtin = require('telescope.builtin')

vim.keymap.set('n', '<leader>pf', builtin.find_files, { desc = "Search all files" })

vim.keymap.set('n', '<leader>pF', builtin.git_files, { desc = "Search git files" })

vim.keymap.set({ 'n', 'v' }, '?', builtin.grep_string, { desc = "Grep String Under Cursor" })
vim.keymap.set('n', '<leader>pg', builtin.live_grep, { desc = "Live Grep" })

vim.keymap.set('n', '<leader>pb', builtin.buffers, { desc = "Search buffers" })

vim.keymap.set('n', '<leader>ph', builtin.help_tags, { desc = "Search Help Tags" })
