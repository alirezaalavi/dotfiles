import dracula.draw

# Load existing settings made via :set
config.load_autoconfig()

config.set("auto_save.session", False)
config.set("changelog_after_upgrade", "minor")
config.set("colors.webpage.preferred_color_scheme", "dark")

dracula.draw.blood(c, {
    'spacing': {
        'vertical': 6,
        'horizontal': 8
    }
})
