#!/bin/bash
## Run scripts using rofi

SCRIPTS_DIR="$HOME/.config/scripts"

# Get a list of script files in the directory
scripts=$(find $SCRIPTS_DIR -type f -executable)

# Create a Rofi menu with the script names
selected_script=$(echo "$scripts" | rofi -dmenu -config $HOME/.config/rofi/rofidmenu.rasi -p "Select a script to run:")

# Run the selected script
if [ -n "$selected_script" ]; then
    $selected_script
fi
