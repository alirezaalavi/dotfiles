#!/bin/bash

# this script is to install my essential packages and apps easily.
# the config files should be handled manually

echo "=========== Updating system ============";
pacman -Syu --noconfirm

echo "=========== Installing basic packages ==========="
pacman -S --needed base-devel git
pacman -S --noconfirm rxvt-unicode pcmanfm gvfs gvfs-afc gvfs-mtp gvfs-smb \
gvfs-gphoto2 xarchiver ranger network-manager-applet ufw arandr autorandr btop \
conky curl dunst fish playerctl wget xfce4-power-manager pavucontrol alsa-utils \
python xautolock xorg-setxkbmap man tlp gnome-keyring libsecret polkit \
polkit-gnome rofi rofi-calc rofi-emoji maim redshift

echo "=========== Installing fonts==========="
pacman -S fontconfig ttf-font-icons terminus-font ttf-dejavu ttf-firacode-nerd ttf-terminus-nerd

echo "=========== Configuring systemd ============"

echo " CONFIGURING TLP "
systemctl enable tlp.service
systemctl mask systemd-rfkill.service systemd-rfkill.socket

echo "============ FINNISHED! ==========="
