#!/bin/bash

# set us,ir as keyboard layouts and switch between them using alt_shift, and map caps lock to escape
setxkbmap -layout 'us,ir' -option 'grp:alt_shift_toggle' -option 'caps:escape'

exit
