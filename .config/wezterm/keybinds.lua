local wezterm = require 'wezterm'
local act = wezterm.action

local keybinds = {
    -- switch back to the last active tab
    {
        key = '/',
        mods = 'LEADER',
        action = act.ActivateLastTab,
    },
    -- Switching panes
    {
        key = 'h',
        mods = 'CTRL|SHIFT',
        action = act.ActivatePaneDirection 'Left',
    },
    {
        key = 'l',
        mods = 'CTRL|SHIFT',
        action = act.ActivatePaneDirection 'Right',
    },
    {
        key = 'k',
        mods = 'CTRL|SHIFT',
        action = act.ActivatePaneDirection 'Up',
    },
    {
        key = 'j',
        mods = 'CTRL|SHIFT',
        action = act.ActivatePaneDirection 'Down',
    },
    -- Adjust pane size (resize pane) | Resing is better done with key_table
    {
        key = 'H',
        mods = 'LEADER',
        action = act.AdjustPaneSize { 'Left', 5 },
    },
    {
        key = 'J',
        mods = 'LEADER',
        action = act.AdjustPaneSize { 'Down', 5 },
    },
    { key = 'K', mods = 'LEADER', action = act.AdjustPaneSize { 'Up', 5 } },
    {
        key = 'L',
        mods = 'LEADER',
        action = act.AdjustPaneSize { 'Right', 5 },
    },
    -- Create new tab
    { key = 'c', mods = 'LEADER', action = act.SpawnTab 'CurrentPaneDomain' },
    -- Activate previous or next tab
    { key = 'n', mods = 'LEADER', action = act.ActivateTabRelative(1) },
    { key = 'N', mods = 'LEADER', action = act.ActivateTabRelative(-1) },
    -- Close Current Pane
    {
        key = 'x',
        mods = 'LEADER',
        action = act.CloseCurrentPane { confirm = false },
    },
    -- Close Current Tab
    {
        key = 'X',
        mods = 'LEADER',
        action = act.CloseCurrentTab { confirm = true },
    },
    -- Move tab relatively
    -- FIXME not working
    { key = '<', mods = 'CTRL|SHIFT', action = act.MoveTabRelative(-1) },
    { key = '>', mods = 'CTRL|SHIFT', action = act.MoveTabRelative(1) },
    -- activate pane selection mode with the default alphabet (labels are "a", "s", "d", "f" and so on)
    { key = 'g', mods = 'LEADER',     action = act.PaneSelect },
    -- show the pane selection mode, but have it swap the active and selected panes
    {
        key = 'G',
        mods = 'LEADER',
        action = act.PaneSelect {
            mode = 'SwapWithActive'
            -- mode = 'SwapWithActiveKeepFocus',
        },
    },
    -- {
    --     key = '!',
    --     mods = 'LEADER|SHIFT',
    --     action = act.PaneSelect {
    --         mode = 'MoveToNewTab'
    --     },
    -- },
    -- Toggle zoom mode for pane
    {
        key = 'z',
        mods = 'LEADER',
        action = act.TogglePaneZoomState,
    },
    { key = 'e',     mods = 'LEADER',     action = act.ShowTabNavigator },
    { key = 'Enter', mods = 'SHIFT|CTRL', action = act.SpawnWindow },

    -- create a new split and run your default program inside it
    {
        key = 'v',
        mods = 'LEADER',
        action = act.SplitHorizontal { domain = 'CurrentPaneDomain' },
    },
    -- create a new split and run your default program inside it
    {
        key = 's',
        mods = 'LEADER',
        action = act.SplitVertical { domain = 'CurrentPaneDomain' },
    },
    { key = 'Space', mods = 'CTRL|SHIFT', action = act.ActivateCopyMode },
    { key = 'r',     mods = 'LEADER',     action = act.ActivateKeyTable { name = 'resize_pane', one_shot = false, }, },
    { key = "o",     mods = "LEADER",     action = act.RotatePanes("Clockwise") },
    { key = "O",     mods = "LEADER",     action = act.RotatePanes("CounterClockwise") },
    -- Rename Tab
    {
        key = "%",
        mods = "LEADER|SHIFT",
        action = act.PromptInputLine({
            description = "Enter new name for tab",
            -- selene: allow(unused_variable)
            ---@diagnostic disable-next-line: unused-local
            action = wezterm.action_callback(function(window, pane, line)
                -- line will be `nil` if they hit escape without entering anything
                -- An empty string if they just hit enter
                -- Or the actual line of text they wrote
                if line then
                    window:active_tab():set_title(line)
                end
            end),
        }),
    },
    {
        key = '!',
        mods = 'LEADER | SHIFT',
        action = wezterm.action_callback(function(win, pane)
            local tab, window = pane:move_to_new_tab()
        end),
    },

    ------------------------
    --  DISABLE DEFAULTS  --
    ------------------------
    {
        key = 'n',
        mods = 'SHIFT|CTRL',
        action = act.DisableDefaultAssignment,
    },
    {
        key = 'x',
        mods = 'SHIFT|CTRL',
        action = act.DisableDefaultAssignment,
    },
}

-- Switch to tab i
for i = 1, 8 do
    -- LEADER + number to activate that tab
    table.insert(keybinds, {
        key = tostring(i),
        mods = 'LEADER',
        action = act.ActivateTab(i - 1),
    })
end

return keybinds
