#!/bin/bash

killall conky;
conky --quiet --daemonize &&
conky --quiet --daemonize -c $HOME/.config/conky/Rischa/Rischa.conf

exit
