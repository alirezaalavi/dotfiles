local wezterm = require 'wezterm'
local keybinds = require 'keybinds'
local key_tables = require 'key_tables'

local config = {
    default_prog = { '/usr/bin/zsh', '-l' },
    --
    -- color_scheme = "Catppuccin Macchiato",
    -- color_scheme = "Dracula",
    color_scheme = "Catppuccin Mocha",
    bidi_enabled = true,

    use_fancy_tab_bar = false,
    automatically_reload_config = false,
    animation_fps = 1,
    window_decorations = "RESIZE",
    window_background_opacity = 0.8,
    window_padding = {
        left = 1,
        right = 1,
        top = 1,
        bottom = 1,
    },
    leader = { key = 'b', mods = 'CTRL', timeout_milliseconds = 10000 },
    font = wezterm.font 'FiraCodeNerdFontMono',

    keys = keybinds,
    key_tables = key_tables,
}

return config
