#!/bin/bash

curl -Lo firefox-dev.tar.bz2 "https://download.mozilla.org/?product=firefox-devedition-latest-ssl&os=linux64&lang=en-US"
tar -xvf firefox-dev.tar.bz2
sudo mv /opt/firefox /opt/firefox.old
sudo mv firefox /opt && sudo rm -rf /opt/firefox.old
rm ./firefox.dev.tar.bz2

# TODO restart firefox
