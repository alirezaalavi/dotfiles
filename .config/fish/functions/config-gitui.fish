function config-gitui --wraps='gitui -d ~/.cfg -w ~' --description 'alias config-gitui=gitui -d ~/.cfg -w ~'
  gitui -d ~/.cfg -w $HOME $argv; 
end
